"""Test all the cookies."""

import json
import subprocess  # noqa: S404
from os import path

import pytest
from pytest_cookies.plugin import Cookies  # type: ignore

from tests.paths import (
    TEMPLATES_PATH,
    file_path_in_result,
    is_dir_in_result,
    read_file_in_result,
)
from tests.toml import TomlExtract

TEMPLATE_PYTHON_POETRY_PATH = path.join(TEMPLATES_PATH, "python_poetry")


def test_python_poetry_defaults(cookies: Cookies) -> None:
    """Test the creation of all my cookies.

    :param cookies: cookiecutter test fixture
    """
    template = str(TEMPLATE_PYTHON_POETRY_PATH)
    bake_result = cookies.bake(template=template)
    project_toml = TomlExtract(file_path_in_result(bake_result, "pyproject.toml"))

    result = [
        bake_result.exception,
        bake_result.exit_code,
        project_toml.is_pyproject_dep("pytest", dev=True),
        is_dir_in_result(bake_result, "tests"),
        project_toml.is_pyproject_dep("behave", dev=True),
        is_dir_in_result(bake_result, "features"),
        project_toml.value_from_toml(
            ["tool", "poetry", "dev-dependencies", "nimoy-framework"]
        ),
        is_dir_in_result(bake_result, "specs"),
        type(json.loads(read_file_in_result(bake_result, ".cookiecutter.json"))),
    ]

    assert result == [None, 0, True, True, False, False, None, False, dict]


@pytest.mark.slow()
def test_python_poetry(cookies: Cookies) -> None:
    """Test template python_poetry with all features.

    :param cookies: cookiecutter test fixture
    """
    template = str(TEMPLATE_PYTHON_POETRY_PATH)
    bake_result = cookies.bake(template=template)
    test_command = (
        "poetry install"
        " && poetry run black . --check --diff"
        " && poetry run isort . --check --diff"
        " && poetry run mypy ."
        " && poetry run flakeheaven lint ."
        " && poetry run a_poetry_project --help"
        " && poetry run a_poetry_project example-click --name World"
        " && poetry run a_poetry_project example-typer"
        " && cd docs && make html"
    )

    result = subprocess.run(  # noqa: S603,S607
        [test_command],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,  # noqa: S602
        cwd=bake_result.project_path,
    )

    if result.returncode != 0:
        print(result.stdout.decode("utf-8"))  # noqa: WPS421
        print(result.stderr.decode("utf-8"))  # noqa: WPS421
    assert result.returncode == 0
