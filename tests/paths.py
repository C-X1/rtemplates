"""Keep test utilities and constants."""

import os

from pytest_cookies.plugin import Cookies  # type: ignore

PROJECT_PATH = os.path.realpath("{}/..".format(os.path.dirname(__file__)))
TEMPLATES_PATH = os.path.join(PROJECT_PATH, "templates")
BUILD_PATH = os.path.join(PROJECT_PATH, "build")


def file_path_in_result(result: Cookies, path: str) -> str:
    """Create full path of project file in cookiecutter result.

    :param result: result of bake
    :param path: path of directory
    :returns: Full path

    return os.path.isfile(path)
    """
    return os.path.join(result.project_path, path)


def is_dir_in_result(result: Cookies, path: str) -> bool:
    """Check if path is a directory in cookiecutter result.

    :param result: result of bake
    :param path: path of directory
    :returns: True if file, False otherwise.

    return os.path.isfile(path)
    """
    return os.path.exists(file_path_in_result(result, path))


def is_file_in_result(result: Cookies, path: str) -> bool:
    """Check if path is a file in cookiecutter result.

    :param result: result of bake
    :param path: path of directory
    :returns: True if file, False otherwise.
    """
    return os.path.exists(file_path_in_result(result, path))


def read_file_in_result(result: Cookies, path: str) -> str:
    """Check if path is a file in cookiecutter result.

    :param result: result of bake
    :param path: path of directory
    :returns: content of file
    """
    with open(file_path_in_result(result, path)) as result_file:
        return result_file.read()
