"""Check values in a toml file."""

import toml


class TomlExtract(object):
    """Extract values from toml."""

    def __init__(self, path: str):
        """Initialize content.

        :param path: Path of toml file
        """
        self.toml_content = toml.load(path)

    def entry_exists(self, keys: list):
        """Check if key exists inside a toml file.

        :param keys: String of the location (like ["tool", "TOOLNAME", "KEY"])
        :returns: True if key is there
        """
        return self.value_from_toml(keys) is not None

    def value_from_toml(self, keys: list):
        """Get value from a location inside a toml file.

        :param keys: String of the location (like ["tool", "TOOLNAME", "KEY"])
        :returns: Value at location
        """
        current_level = self.toml_content
        for key in keys:
            if key not in current_level:
                return None
            current_level = current_level[key]

        return current_level

    def is_pyproject_dep(self, dependency: str, dev: bool = False):
        """Check if dependency is in a pyproject.toml file.

        Precondition: File from init must be pyproject.toml

        :param dependency: The depenency to check for
        :param dev: Check dev dependencies instead of normal.
        :returns: True if dependency is present, False otherwise
        """
        prefix = "dev-" if dev else ""
        return self.entry_exists(
            ["tool", "poetry", f"{prefix}dependencies", dependency]
        )
