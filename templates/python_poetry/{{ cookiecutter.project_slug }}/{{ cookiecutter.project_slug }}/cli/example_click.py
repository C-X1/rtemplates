"""Implement click example."""

import click


@click.command()
@click.option(
    "--name", prompt="The name to greet", help="The one which will be greeted"
)
def example_click(name: str) -> None:
    """Great somebody.

    :param name: The name of the person to greet.
    """
    click.echo(f"Hello {name}!")
