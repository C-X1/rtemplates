"""Implement typer example."""

from {{cookiecutter.project_slug}}.cli.app import app


@app.command()
def example_typer() -> None:
    """Be an example for typer."""
