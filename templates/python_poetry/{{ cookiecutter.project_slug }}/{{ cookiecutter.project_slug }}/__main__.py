"""Make {{ cookiecutter.project_slug }} executable by `python -m {{ cookiecutter.project_slug }}`."""
from {{cookiecutter.project_slug}} import {{ cookiecutter.project_slug }}

if __name__ == "__main__":  # pragma: no cover
    {{ cookiecutter.project_slug }}.main()
