{%- set version = cookiecutter.python_version.split(".") %}
{%- set pyenv = "py" + version[0] + version[1] %}
{%- set envs = ["black,coverage,isort,lint,mypy"] -%}
{%- set test_suits = [] -%}
{%- set test_dirs = [] -%}
{%- if cookiecutter.use_pytest == "yes" -%}
    {%- set _ = test_dirs.append("tests") -%}
    {%- set _ = envs.append("pytest") -%}
    {%- set _ = test_suits.append("pytest") -%}
{%- endif -%}
{%- if cookiecutter.use_behave == "yes" -%}
    {%- set _ = test_dirs.append("features") -%}
    {%- set _ = envs.append("behave") -%}
    {%- set _ = test_suits.append("behave") -%}
{%- endif -%}
{%- if cookiecutter.use_nimoy == "yes" -%}
    {%- set _ = test_dirs.append("specs") -%}
    {%- set _ = envs.append("nimoy") -%}
    {%- set _ = test_suits.append("nimoy") -%}
{%- endif -%}
{%- set code_dirs = [cookiecutter.project_slug] + test_dirs -%}
[tox]
envlist = clean,{{ pyenv }},{{ envs|join(",") }}
isolated_build = True
requires =
    tox-poetry-installer[poetry] >= 0.9.0

[testenv:{% raw %}{{% endraw %}{{ envs|join(",") }}{% raw %}}{% endraw %}]
envdir = {toxworkdir}/.work_env
allowlist_externals =
    mkdir
    rm
    sh
usedevelop = True
install_dev_deps = True
commands =
{%- if cookiecutter.use_behave == "yes" %}
    behave: coverage run --rcfile features/.coveragerc --source="." -m behave
    {%- if cookiecutter.support_allure == "yes" %} -f allure_behave.formatter:AllureFormatter -o {toxinidir}/build/reports/features{%- endif %} ./features
{%- endif %}
    black: black {{ code_dirs|join(" ") }} --check --diff
{%- for test_dir in test_dirs %}
    coverage: sh -c "coverage combine --rcfile {{ test_dir }}/.coveragerc || echo 'ignored error'"
    coverage: coverage html --rcfile {{ test_dir }}/.coveragerc
{%- endfor %}
    coverage: coverage combine --keep --data-file={toxinidir}/build/reports/coverage/combined_coverage
    {%- for test_suit in test_suits %} {toxinidir}/build/reports/coverage/{{ test_suit }}_coverage{%- endfor %}
    coverage: coverage html --data-file {toxinidir}/build/reports/coverage/combined_coverage --title "Combined Coverage" -d {toxinidir}/build/reports/coverage/combined_html
{%- if cookiecutter.support_allure == "yes" %}
    {%- for test_suit in test_suits %}
    coverage: rpycihelpers allure-coverage --input-path {toxinidir}/build/reports/coverage/{{ test_suit }}_html --output-path {toxinidir}/build/reports/coverage/{{ test_suit }}_allure
    {%- endfor %}
    coverage: rpycihelpers allure-coverage --input-path {toxinidir}/build/reports/coverage/combined_html --output-path {toxinidir}/build/reports/coverage/combined_allure
{%- endif %}
    lint: flakeheaven lint {{ code_dirs|join(" ") }} --format default --output-file {toxinidir}/build/reports/lint/flake.txt --no-show-source
    lint: flake8_junit {toxinidir}/build/reports/lint/flake.txt {toxinidir}/build/reports/lint/flake.xml
    mypy: mkdir -p {toxinidir}/build/reports/mypy
    mypy: mypy {{ code_dirs|join(" ") }} --junit-xml {toxinidir}/build/reports/mypy/mypy.xml
    isort: isort {{ code_dirs|join(" ") }} --check --diff
{%- if cookiecutter.use_nimoy == "yes" %}
    nimoy:  coverage run --rcfile features/.coveragerc --source="." -m nimoy
{%- endif %}
{%- if cookiecutter.use_pytest == "yes" %}
    pytest: pytest --cov {{ cookiecutter.project_slug }} --cov tests --cov-append --cov-branch --cov-config tests/.coveragerc
    {%- if cookiecutter.support_allure == "yes" %} --alluredir={toxinidir}/build/reports/tests{%- endif %} --basetemp="{envtmpdir}" {posargs}
{%- endif %}

depends =
    {coverage}: clean,{{ test_suits|join(",") }}

[testenv:clean]
skip_install = true
allowlist_externals =
     rm
commands=
    rm {toxinidir}/build/ -rf
