"""Post process python_poetry template output."""
import os
from shutil import rmtree

OUTPUT_DIRECTORY = os.path.realpath(os.getcwd())


def remove(path: str) -> None:
    """Remove file or directory.

    :param path: The path to remove.
    """
    rmtree(os.path.join(OUTPUT_DIRECTORY, path))


def main() -> None:
    """Do post processing."""
    if "{{ cookiecutter.use_behave }}" == "no":
        remove("features")

    if "{{ cookiecutter.use_pytest }}" == "no":
        remove("tests")

    if "{{ cookiecutter.use_nimoy }}" == "no":
        remove("specs")


if __name__ == "__main__":
    main()
